package com.assentis.training.infrastructure.jpa;

import com.assentis.training.domain.BaseEntity;


public class GenericJpaRepositoryForBaseEntity<E extends BaseEntity> extends GenericJpaRepository<E, Long>{
		
	public void delete(Long id){
		E entity = load(id);
		entity.markAsRemoved();		
		save(entity);	
	}
}

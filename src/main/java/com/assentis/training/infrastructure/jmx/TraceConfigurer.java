package com.assentis.training.infrastructure.jmx;

import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

@Component
@ManagedResource(description = "Enable and disable tracing implemented by aspect")
public class TraceConfigurer {

	private boolean traceEnabled = false;

	@ManagedOperation(description="Check current traceing status")
	public boolean isTraceEnabled() {
		return traceEnabled;
	}
	@ManagedOperation(description="Enable or disable tracing")
	public void setTraceEnabled(boolean traceEnabled) {
		this.traceEnabled = traceEnabled;
	}
}

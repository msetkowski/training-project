package com.assentis.training.infrastructure.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.assentis.training.infrastructure.jmx.TraceConfigurer;

@Aspect
@Component
public class TraceAspect {
	@Autowired
	private TraceConfigurer traceConfigurer;

	private Logger logger = Logger.getLogger(TraceAspect.class.getName());

	@Around(value = "bean(*Bean)")
	public Object trace(ProceedingJoinPoint joinPoint) throws Throwable {
		long startTime = 0L;
		if (traceConfigurer.isTraceEnabled() && logger.isTraceEnabled()) {
			logger.trace("Method " + joinPoint.getSignature() + "is called");
			startTime = System.currentTimeMillis();
		}
		try {
			return joinPoint.proceed();
		} catch (Throwable t) {
			throw t;
		} finally {
			if (traceConfigurer.isTraceEnabled() && logger.isTraceEnabled()) {
				long ellapsedTime = System.currentTimeMillis() - startTime;
				logger.trace("Method " + joinPoint.getSignature()
						+ "is finished, ellapse time: " + ellapsedTime + "[ms]");
			}
		}
	}
}

package com.assentis.training.infrastructure.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.assentis.training.domain.Product;
import com.assentis.training.domain.ProductCatalog;
import com.assentis.training.infrastructure.repository.CatalogRepository;
import com.assentis.training.infrastructure.repository.ProductRepository;

@Component("productCatalogService")
@Transactional
public class ProductCatalogService {

	@Autowired
	private CatalogRepository catalogRepository;
	@Autowired
	private ProductRepository productRepository;
	
	public List<ProductCatalog> getAllCatalogs(){
		return catalogRepository.getAllCatalogs();
	}

	public List<Product> getProductsById(Long itemId) {
		return productRepository.getProductsByType(itemId);
	}
}

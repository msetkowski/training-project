package com.assentis.training.infrastructure.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.assentis.training.domain.Buyer;
import com.assentis.training.domain.BuyerFactory;
import com.assentis.training.infrastructure.repository.BuyerRepository;
import com.assentis.training.presentation.dto.BuyerDto;

@Component("buyerService")
@Transactional
public class BuyerService {
	@Autowired
	private BuyerRepository buyerRepository;

	public List<BuyerDto> getAllBuyers() {
		List<BuyerDto> result = new ArrayList<BuyerDto>();
		List<Buyer> buyers = buyerRepository.getAllBuyers();
		for (Buyer buyer : buyers) {
			result.add(new BuyerDto()
				.setName(buyer.getName())
				.setSurname(buyer.getSurname())
				.setId(buyer.getEntityId()));
		}
		return result;
	}
	
	public void createNewBuyer(BuyerDto buyer){
		Buyer newBuyer = BuyerFactory.createNewBuyer();
		newBuyer.setName(buyer.getName());
		newBuyer.setSurname(buyer.getSurname());
		
		newBuyer.getAddress().setCity(buyer.getCity());
		newBuyer.getAddress().setHouseNumber(buyer.getHouseNumber());
		newBuyer.getAddress().setPostCode(buyer.getPostCode());
		newBuyer.getAddress().setStreet(buyer.getStreet());
		
		String[] parts = buyer.getEmail().split("@");
		newBuyer.getEmail().setIdentifier(parts[0]);
		newBuyer.getEmail().setHost(parts[1]);
		buyerRepository.persist(newBuyer);
		
	}
}

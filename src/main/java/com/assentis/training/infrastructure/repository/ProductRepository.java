package com.assentis.training.infrastructure.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.assentis.training.domain.Product;
import com.assentis.training.infrastructure.jpa.GenericJpaRepositoryForBaseEntity;

@Repository
public class ProductRepository extends GenericJpaRepositoryForBaseEntity<Product> {
	
	@SuppressWarnings("unchecked")
	public List<Product> getProductsByType(Long productTypeId){
		String query = "select p from Product p where p.productTypeId=:productTypeId";
		return entityManager.createQuery(query)
				.setParameter("productTypeId", productTypeId)
				.getResultList();
	}
}

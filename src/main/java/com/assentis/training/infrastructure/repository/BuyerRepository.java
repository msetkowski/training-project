package com.assentis.training.infrastructure.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.assentis.training.domain.Buyer;
import com.assentis.training.infrastructure.jpa.GenericJpaRepositoryForBaseEntity;

@Repository
public class BuyerRepository extends GenericJpaRepositoryForBaseEntity<Buyer> {

	@SuppressWarnings("unchecked")
	public List<Buyer> getAllBuyers() {
		return entityManager.createQuery("select b from Buyer b").getResultList();

	}

}

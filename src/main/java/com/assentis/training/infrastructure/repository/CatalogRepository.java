package com.assentis.training.infrastructure.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.assentis.training.domain.ProductCatalog;
import com.assentis.training.infrastructure.jpa.GenericJpaRepositoryForBaseEntity;

@Repository
public class CatalogRepository extends GenericJpaRepositoryForBaseEntity<ProductCatalog> {

	@SuppressWarnings("unchecked")
	public List<ProductCatalog> getAllCatalogs(){
		return entityManager.createQuery("select c from ProductCatalog c").getResultList();
	}
}

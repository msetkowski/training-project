package com.assentis.training.application.beans;

import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.richfaces.component.UITree;
import org.richfaces.component.html.HtmlTree;
import org.richfaces.event.NodeSelectedEvent;
import org.richfaces.model.TreeNode;
import org.richfaces.model.TreeNodeImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.assentis.training.domain.CatalogEntry;
import com.assentis.training.domain.Product;
import com.assentis.training.domain.ProductCatalog;
import com.assentis.training.domain.ProductType;
import com.assentis.training.infrastructure.service.ProductCatalogService;

@Controller("productCatalogBean")
@Scope("request")
public class ProductCatalogBean {
	
	private class TreeData{
		private String name;
		private long id;
		private boolean productType;

		public TreeData(String name, long id, boolean productType){
			this.name = name;
			this.id = id;
			this.productType = productType;
		}
		
		boolean isProductType(){
			return productType;
		}

		public long getId(){
			return id;
		}
		
		@Override
		public String toString() {
			return name;
		}
	}
	
	@Autowired
	private ProductCatalogService catalogService;
	protected UITree treeBinding;
	private TreeNode<?> rootNode = null;
	private List<Product> products = Collections.emptyList();
	
	@PostConstruct
	public void initialize(){
		loadTree();
	}
	
	

	public UITree getTreeBinding() {
		return treeBinding;
	}

	

	public void setTreeBinding(UITree treeBinding) {
		this.treeBinding = treeBinding;
	}

	public void processSelection(NodeSelectedEvent event) {
		HtmlTree tree = (HtmlTree) event.getComponent();
		Long itemId = ((TreeData) tree.getRowData()).getId();
		boolean isProduct = ((TreeData) tree.getRowData()).isProductType();

		TreeNode<?> currentNode = tree.getModelTreeNode(tree.getRowKey());
		if (currentNode.isLeaf() && isProduct) {
			products = catalogService.getProductsById(itemId);
		}
	}

	public TreeNode<?> getTreeNode() {
		if (rootNode == null) {
			loadTree();
		}
		return rootNode;
	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void loadTree() {

		rootNode = new TreeNodeImpl();
		List<ProductCatalog> catalogs = catalogService.getAllCatalogs();
		for (ProductCatalog productCatalog : catalogs) {
			TreeNode catalogNode = new TreeNodeImpl();
			catalogNode.setData(new TreeData(productCatalog.getName(), productCatalog.getEntityId(), false));
			List<CatalogEntry> entries = productCatalog.getCatalogEntries();
			for (CatalogEntry catalogEntry : entries) {
				TreeNode catalogEntryNode = new TreeNodeImpl();
				catalogEntryNode.setData(new TreeData(catalogEntry.getCategory(), catalogEntry.getEntityId(), false));
				List<ProductType> types = catalogEntry.getProductType();
				for (ProductType productType : types) {
					TreeNode productTypeNode = new TreeNodeImpl();
					productTypeNode.setData(new TreeData(productType.getName(), productType.getEntityId(), true));
					catalogEntryNode.addChild(productType.getEntityId(), productTypeNode);
				}		
				catalogNode.addChild(catalogEntry.getEntityId(), catalogEntryNode);
			}
			rootNode.addChild(productCatalog.getEntityId(), catalogNode);
		}
	}



	public List<Product> getProducts() {
		return products;
	}
	
}

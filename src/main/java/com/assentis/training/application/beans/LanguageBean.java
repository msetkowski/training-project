package com.assentis.training.application.beans;

import java.io.Serializable;
import java.util.Locale;

import javax.faces.context.FacesContext;

public class LanguageBean implements Serializable{

	private static final long serialVersionUID = -5699829577457612059L;
	
	public LanguageBean(){
		selectEnglish();
	}

	public void selectEnglish(){
		FacesContext.getCurrentInstance().getViewRoot().setLocale(Locale.ENGLISH);
	}
	
	public void selectPolish(){
		FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale("pl"));
	}
}

package com.assentis.training.application.beans;

import java.io.Serializable;

import javax.faces.context.FacesContext;

public class SkinBean implements Serializable{

	private static final long serialVersionUID = -7366552293133684883L;
	private String skin;

	public String getSkin() {
		return skin;
	}

	public void setSkin(String skin) {
		this.skin = skin;
	}

	public void changeSkin() {
		setSkin(FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap().get("newSkin"));
		FacesContext.getCurrentInstance().renderResponse();

	}
}

package com.assentis.training.application.beans;

import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.assentis.training.infrastructure.service.BuyerService;
import com.assentis.training.presentation.dto.BuyerDto;

@Controller("createBuyerBean")
@Scope("request")
public class CreateBuyerBean {
	@Inject
	private BuyerService buyerService;
	
	private String name;
	private String surname;
	private String email;
	private String street;
	private String city;
	private String houseNumber;
	private Long postCode;
	
	public String save(){
		BuyerDto dto = new BuyerDto()
			.setName(name)
			.setSurname(surname)
			.setEmail(email)
			.setCity(city)
			.setStreet(street)
			.setPostCode(postCode)
			.setHouseNumber(houseNumber);
		buyerService.createNewBuyer(dto);
		return "save";
	}
	
	public String cancel(){
		return "cancel";
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	public Long getPostCode() {
		return postCode;
	}
	public void setPostCode(Long postCode) {
		this.postCode = postCode;
	}
}

package com.assentis.training.application.beans;

import java.io.Serializable;


public class MenuBean implements Serializable {

	private static final long serialVersionUID = -9128544297684696705L;

	public String doNew(){
		return "newBuyer";
	}
	
	public String doList(){
		return "listBuyer";
	}
	
	public String doProductCatalog(){
		return "productCatalog";
	}
}

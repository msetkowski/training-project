package com.assentis.training.application.beans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.richfaces.component.html.HtmlExtendedDataTable;
import org.richfaces.model.selection.SimpleSelection;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.assentis.training.infrastructure.service.BuyerService;
import com.assentis.training.presentation.dto.BuyerDto;

@Controller("buyerBean")
@Scope("request")
public class BuyerBean {

	private HtmlExtendedDataTable dataTable;
	private SimpleSelection selection;

	@Inject
	private BuyerService buyerService;

	private List<BuyerDto> buyers;
	
	@PostConstruct
	public void initialize() {
		buyers = buyerService.getAllBuyers();
	}
	
	public List<BuyerDto> getBuyers() {
		return buyers;
	}

	public HtmlExtendedDataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(HtmlExtendedDataTable dataTable) {
		this.dataTable = dataTable;
	}
	
	public SimpleSelection getSelection() {
		return selection;
	}

	public void setSelection(SimpleSelection selection) {
		this.selection = selection;
	}

	public int getRows(){
		return buyers.size();
	}
	
	public void setBuyerService(BuyerService buyerService) {
		this.buyerService = buyerService;
	}

	public void edit(ActionEvent event) {

	}

	public void delete(ActionEvent event) {

	}
}

package com.assentis.training.presentation.dto;

public class BuyerDto {
	private String name;
	private String surname;
	private String email;
	private String city;
	private long id;
	private String street;
	private Long postCode;
	private String houseNumber;
	
	
	public long getId() {
		return id;
	}
	public BuyerDto setId(long id) {
		this.id = id;
		return this;
	}
	public String getName() {
		return name;
	}
	public BuyerDto setName(String name) {
		this.name = name;
		return this;
	}
	public String getSurname() {
		return surname;
	}
	public BuyerDto setSurname(String surname) {
		this.surname = surname;
		return this;
	}
	public String getEmail() {
		return email;
	}
	public BuyerDto setEmail(String email) {
		this.email = email;
		return this;
	}
	public String getCity() {
		return city;
	}
	public BuyerDto setCity(String city) {
		this.city = city;
		return this;
	}
	public BuyerDto setStreet(String street) {
		this.street = street;
		return this;
		
	}
	public BuyerDto setPostCode(Long postCode) {
		this.postCode = postCode;
		return this;
		
	}
	public BuyerDto setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
		return this;	
	}
	public String getStreet() {
		return street;
	}
	public Long getPostCode() {
		return postCode;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
}

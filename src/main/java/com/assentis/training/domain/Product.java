package com.assentis.training.domain;

import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
public class Product extends BaseEntity {
	private String name;
	private Money price;
	private SerialNumber serialNumber;
	private Integer quantity;
	private List<ProductFeature> productFeatures;
	private Long productTypeId;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Money getPrice() {
		return price;
	}
	public void setPrice(Money price) {
		this.price = price;
	}
	
	@Embedded
	public SerialNumber getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(SerialNumber serialNumber) {
		this.serialNumber = serialNumber;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	@OneToMany
	@LazyCollection(LazyCollectionOption.FALSE)
	public List<ProductFeature> getProductFeatures() {
		return productFeatures;
	}
	public void setProductFeatures(List<ProductFeature> productFeatures) {
		this.productFeatures = productFeatures;
	}
	public Long getProductTypeId() {
		return productTypeId;
	}
	public void setProductTypeId(Long productTypeId) {
		this.productTypeId = productTypeId;
	}
}

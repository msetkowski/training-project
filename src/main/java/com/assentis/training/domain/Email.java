package com.assentis.training.domain;

import javax.persistence.Entity;

@Entity
public class Email extends BaseEntity {
	private static final String SEPARATOR = "@";
	private String identifier;
	private String host;
	
	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	@Override
	public String toString() {
		return new StringBuffer().append(identifier).append(SEPARATOR).append(host).toString();
	}
	
	

}

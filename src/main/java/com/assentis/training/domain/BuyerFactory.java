package com.assentis.training.domain;

public class BuyerFactory {
	public static Buyer createNewBuyer(){
		Buyer buyer = new Buyer();
		buyer.setAddress(new Address());
		buyer.setEmail(new Email());
		return buyer;
	}
}

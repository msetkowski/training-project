package com.assentis.training.domain;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class ProductIdentifier implements Serializable {

	private static final long serialVersionUID = 5447893460954817255L;
	private String productIdentifier;

	public String getProductIdentifier() {
		return productIdentifier;
	}

	public void setProductIdentifier(String productIdentifier) {
		this.productIdentifier = productIdentifier;
	}
	
}

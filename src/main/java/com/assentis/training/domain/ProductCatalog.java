package com.assentis.training.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
public class ProductCatalog extends BaseEntity {

	private String name;
	private List<CatalogEntry> catalogEntries;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@OneToMany
	@LazyCollection(LazyCollectionOption.FALSE)
	public List<CatalogEntry> getCatalogEntries() {
		return catalogEntries;
	}
	public void setCatalogEntries(List<CatalogEntry> catalogEntries) {
		this.catalogEntries = catalogEntries;
	}
	
	
	
}

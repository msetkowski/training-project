package com.assentis.training.domain.exception;

public class OrderOperationException extends RuntimeException {

	private static final long serialVersionUID = -2174730695630862255L;

	public OrderOperationException(String string) {
		super(string);
	}

}

package com.assentis.training.domain;

import javax.persistence.Embedded;
import javax.persistence.Entity;

@Entity
public class ProductType extends BaseEntity {

	private ProductIdentifier productIdentifier;
	private String name;
	private String description;
	
	@Embedded
	public ProductIdentifier getProductIdentifier() {
		return productIdentifier;
	}
	public void setProductIdentifier(ProductIdentifier productIdentifier) {
		this.productIdentifier = productIdentifier;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}

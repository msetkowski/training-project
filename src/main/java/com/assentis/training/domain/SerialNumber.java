package com.assentis.training.domain;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class SerialNumber implements Serializable{

	private static final long serialVersionUID = -3868702735783909168L;
	private String serialNumber;

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	@Override
	public String toString() {
		return serialNumber;
	}
}

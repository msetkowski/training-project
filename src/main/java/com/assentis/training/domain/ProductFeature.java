package com.assentis.training.domain;

import javax.persistence.Entity;

@Entity
public class ProductFeature extends BaseEntity {
	private String name;
	private String description;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}

insert into Address (city, street, postcode, housenumber)  values('Zug', 'Zugerbergstrasse',6300, '1A');
insert into Address (city, street, postcode, housenumber)  values('Munchen', 'Kirchenstrasse',5670, '35');

insert into Email (identifier, host) values('blurp','web.de');
insert into Email (identifier, host) values('asterix','hotmail.pl');

insert into Buyer (name, surname, address_entityId, email_entityId) values ('John','Wolf',1,1);
insert into Buyer (name, surname, address_entityId, email_entityId) values ('Jony','Bravo',2,2);

insert into ProductCatalog (name) values ('Standard');
insert into ProductCatalog (name) values ('Extended');

insert into CatalogEntry (category, description) values ('Operating systems','...');
insert into CatalogEntry (category, description) values ('Tools','...');

insert into ProductType (name,productIdentifier) values ('Microsoft Windows', 'OS/2012/99');
insert into ProductType (name,productIdentifier) values ('Ubuntu Linux', 'ST/2012/99');
insert into ProductType (name,productIdentifier) values ('Adobe Something', 'G/2012/91');

insert into ProductCatalog_CatalogEntry (ProductCatalog_entityId,catalogEntries_entityId) values (1,1);
insert into ProductCatalog_CatalogEntry (ProductCatalog_entityId,catalogEntries_entityId) values (1,2);

insert into CatalogEntry_ProductType (CatalogEntry_entityId, productType_entityId) values (1,1);
insert into CatalogEntry_ProductType (CatalogEntry_entityId, productType_entityId) values (1,2);
insert into CatalogEntry_ProductType (CatalogEntry_entityId, productType_entityId) values (2,3);

insert into Product (name, currencyCode, value, quantity, serialNumber, productTypeId) values ('Windows 7 Ultimate', 'EU',199.00,999,'SN:9823465487365874',1);
insert into Product (name, currencyCode, value, quantity, serialNumber, productTypeId) values ('Windows 7 Home', 'EU',119.00,909,'SN:9829995487365874',1);
insert into Product (name, currencyCode, value, quantity, serialNumber, productTypeId) values ('Windows XP', 'EU',129.00,90,'SN:9823465487ADE874',1);
insert into Product (name, currencyCode, value, quantity, serialNumber, productTypeId) values ('Ubuntu 11', 'EU',89.00,999,'SN:98234654873LN874',2);
insert into Product (name, currencyCode, value, quantity, serialNumber, productTypeId) values ('Ubuntu 12', 'EU',99.00,999,'SN:982EA654873LN874',2);
insert into Product (name, currencyCode, value, quantity, serialNumber, productTypeId) values ('Lightroom', 'EU',59.00,999,'SN:982EA5ED873LN874',3);

insert into ProductFeature (name, description) values ('Minimal Hardware','');
insert into ProductFeature (name, description) values ('64bit','');
insert into ProductFeature (name, description) values ('Extra manual','');

insert into Product_ProductFeature (Product_entityId, productFeatures_entityId) values (1,1);
insert into Product_ProductFeature (Product_entityId, productFeatures_entityId) values (3,2);
insert into Product_ProductFeature (Product_entityId, productFeatures_entityId) values (6,3);
